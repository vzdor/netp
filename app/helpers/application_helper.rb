module ApplicationHelper
  def show_date(dt)
    dt
      .to_date
      .to_s(:long)
  end
end

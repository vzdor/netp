class Expense < ActiveRecord::Base
  has_many :user_expenses

  validates :amount, presence: true

  after_initialize :set_defaults

  def set_defaults
    self.deduction ||= 0
  end
end

class User < ActiveRecord::Base
  has_many :payments

  has_many :user_expenses

  validates :username, presence: true, uniqueness: true

  validates :deduction, presence: true

  validates :starting_balance, numericality: true

  scope :enabled, -> { where(is_disabled: false) }

  def balance
    @balance ||= get_balance
  end

  def to_param
    [id, username].join('-')
  end

  def get_balance
    starting_balance +
      (payments.sum(:amount) || 0.0) -
      (user_expenses.sum(:amount) || 0.0)
  end
end

class UsersController < ApplicationController
  before_action :bill_monthly

  before_action :get_user, only: [:show, :edit, :update]

  def index
    q = User
    unless params[:all]
      q = q.where(is_disabled: false)
    end
    q = q.order("is_disabled asc, username asc")
    @users = UserDecorator.from_ary(q)
    @total_balance = Payment.sum(:amount) - Expense.sum(:amount)
  end

  def show
    payments = @user.payments.order(created_at: :desc)
    payment = Payment.new(amount: @user.deduction)
    user = UserDecorator.new(@user)
    render locals: {user: user, payments: payments, payment: payment}
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'Updated.'
    else
      render :edit
    end
  end

  def new
    @user = User.new(is_disabled: false, deduction: 40)
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to user_path(@user), notice: 'Added.'
    else
      render :new
    end
  end

  private

  def get_user
    @user = User.find(params[:id])
  end

  def bill_monthly
    MonthlyBiller.bill!
  end

  def user_params
    params
      .require(:user)
      .permit(:username, :deduction, :is_disabled, :starting_balance)
  end
end

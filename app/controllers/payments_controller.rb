class PaymentsController < ApplicationController
  def create
    user = User.find(params[:user_id])
    @payment = user.payments.build(payment_params)
    if @payment.save
      redirect_to user, notice: 'Payment added successfully'
    else
      # todo:
    end
  end

  protected

  def payment_params
    params
      .require(:payment)
      .permit(:amount)
  end
end

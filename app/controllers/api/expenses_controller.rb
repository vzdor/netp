class Api::ExpensesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    expense = Expense.new(expense_params)
    if expense.save
      biller = UserBiller.new(expense)
      biller.bill!
      head(:ok)
    else
      # xxx
    end
  end

  protected

  def expense_params
    params
      .require(:expense)
      .permit(:amount, :deduction)
  end
end

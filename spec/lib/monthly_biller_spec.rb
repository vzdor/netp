require 'rails_helper'

RSpec.describe MonthlyBiller do
  describe 'bill!' do
    it 'makes an expense for each missing month' do
      create(:expense, created_at: 2.month.ago)
      expect { MonthlyBiller.bill! }.to change(Expense, :count).by(2)
    end

    it 'keeps bill day' do
      create(:expense, created_at: 1.month.ago)
      MonthlyBiller.bill!
      expense = Expense.last
      expect(expense.created_at.day).to eq(Time.now.day)
    end

    it 'bills user' do
      user = create(:user)
      create(:expense, created_at: 2.month.ago)
      MonthlyBiller.bill!
      expect(user.balance).to eq(- user.deduction * 2)
    end

    it 'sets new expense amount' do
      create(:expense, created_at: 1.month.ago)
      MonthlyBiller.bill!
      expect(Expense.last.amount).to eq(200)
    end
  end
end

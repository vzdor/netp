require 'rails_helper'

RSpec.describe UserDecorator do
  describe '#status' do
    context 'when debt is 3 times the deduction' do
      it 'returns freeloader' do
        user = create(:user, starting_balance: -200, deduction: 50)
        decorator = UserDecorator.new(user)
        expect(decorator.status).to include("freeloader")
      end
    end
  end
end

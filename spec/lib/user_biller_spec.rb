require 'rails_helper'

RSpec.describe UserBiller do
  describe '#bill!' do
    it 'adds a new user expense' do
      user = create(:user, deduction: 25)
      expense = create(:expense, amount: 100)
      biller = UserBiller.new(expense)
      expect { biller.bill! }.to change(user.user_expenses, :count).by(1)
      e = user.user_expenses.first
      expect(e.amount).to eq(25)
    end

    context 'when we want to bill by an amount' do
      it 'adds a new user expense with that amount' do
        user = create(:user)
        expense = create(:expense, amount: 500, deduction: 123)
        biller = UserBiller.new(expense)
        biller.bill!
        e = user.user_expenses.first
        expect(e.amount).to eq(123)
      end
    end
  end
end

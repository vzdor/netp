FactoryGirl.define do
  factory :user do
    sequence(:username) { |n| "user-#{n}" }
    starting_balance 0.0
    deduction 50.0
    is_disabled false
  end
end

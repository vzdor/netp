FactoryGirl.define do
  factory :user_expense do
    user
    expense
    amount 100
  end
end

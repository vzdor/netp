require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  render_views

  describe '#index' do
    it 'renders successfully' do
      create(:user)
      get :index
      expect(response).to be_success
    end

    it 'bills monthly' do
      expect(MonthlyBiller).to receive(:bill!)
      create(:user)
      get(:index)
    end
  end

  describe '#show' do
    it 'renders successfully' do
      user = create(:user)
      get :show, id: user
      expect(response).to be_success
    end
  end

  describe '#edit' do
    it 'shows the edit form' do
      user = create(:user)
      get :edit, id: user
      expect(response).to be_success
    end
  end

  describe '#update' do
    def do_update(user, attributes = {})
      defaults = {deduction: 123, is_disabled: false}
      put :update, id: user, user: defaults.merge(attributes)
    end

    it 'updates the user' do
      user = create(:user, deduction: 1, is_disabled: true)
      do_update(user)
      expect { user.reload }.to change(user, :deduction)
      expect(response).to be_redirect
    end
  end

  describe '#create' do
    it 'adds a user' do
      user = build(:user)
      expect { post :create, user: user.attributes }.to change(User, :count)
      expect(response).to be_redirect
    end
  end

  describe '#new' do
    it 'shows the new user form' do
      get :new
      expect(response).to be_success
    end
  end
end

require 'rails_helper'

RSpec.describe Api::ExpensesController, :type => :controller do
  describe '#create' do
    it 'creates a new expense' do
      user = create(:user, deduction: 50)
      expect { get :create, expense: {amount: 100} }.to change(Expense, :count).by(1)
      expect(user.balance).to eq(-50)
    end

    it 'uses specified deduction' do
      user = create(:user)
      expect { get :create, expense: {amount: 100, deduction: 10} }.to change(Expense, :count).by(1)
      expect(user.balance).to eq(-10)
    end
  end
end

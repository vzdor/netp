require 'rails_helper'

RSpec.describe PaymentsController, :type => :controller do
  render_views

  describe '#create' do
    it 'creates a new payment' do
      user = create(:user)
      expect { post :create, user_id: user, payment: {amount: 100} }.to change(user.payments, :count).by(1)
      expect(response).to redirect_to(user_path(user))
    end
  end
end

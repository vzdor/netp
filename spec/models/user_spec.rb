require 'rails_helper'

RSpec.describe User, :type => :model do
  describe '#balance' do
    it 'adds starting balance' do
      user = create(:user, starting_balance: 100)
      expect(user.balance).to eq(100)
    end

    it 'deducts expenses' do
      user = create(:user)
      create(:user_expense, user: user, amount: 100)
      create(:user_expense, user: user, amount: 50)
      expect(user.balance).to eq(-150)
    end

    it 'adds payments' do
      user = create(:user)
      create(:payment, user: user, amount: 100)
      create(:payment, user: user, amount: 50)
      expect(user.balance).to eq(150)
    end
  end

  describe 'enabled scope' do
    it 'returns enabled users' do
      create(:user)
      create(:user, is_disabled: true)
      expect(User.enabled.size).to eq(1)
    end
  end

  describe 'validations' do
    it 'requires deduction' do
      user = User.new
      expect(user).to_not be_valid
      expect(user.errors).to include(:deduction)
    end
  end
end

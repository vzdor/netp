class RemoveCreatedByFromExpenses < ActiveRecord::Migration
  def change
    remove_column :expenses, :created_by, :integer, null: false
  end
end

class RenameExpensesPerUserDeductionToDeduction < ActiveRecord::Migration
  def change
    rename_column :expenses, :per_user_deduction, :deduction
  end
end

class RemoveCreatedByFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :created_by, :integer, null: false
  end
end

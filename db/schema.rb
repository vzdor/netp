# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141206121519) do

  create_table "expenses", force: true do |t|
    t.decimal  "amount",      precision: 16, scale: 2, null: false
    t.decimal  "deduction",   precision: 16, scale: 2, null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "from_id",                    null: false
    t.text     "content",                    null: false
    t.boolean  "is_hidden",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: true do |t|
    t.integer  "user_id",                              null: false
    t.decimal  "amount",      precision: 16, scale: 2, null: false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: true do |t|
    t.integer "tag_id",        null: false
    t.integer "taggable_id",   null: false
    t.string  "taggable_type", null: false
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type"], name: "index_taggings_on_tag_id_and_taggable_id_and_taggable_type", unique: true, using: :btree

  create_table "tags", force: true do |t|
    t.string "name", null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "user_expenses", force: true do |t|
    t.integer  "user_id",                             null: false
    t.integer  "expense_id",                          null: false
    t.decimal  "amount",     precision: 16, scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username",         limit: 64,                                          null: false
    t.string   "password"
    t.string   "name"
    t.string   "address"
    t.string   "phone"
    t.string   "email"
    t.boolean  "is_super",                                             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "starting_balance",            precision: 16, scale: 2, default: 0.0,   null: false
    t.decimal  "deduction",                   precision: 16, scale: 2
    t.boolean  "is_disabled",                                          default: false
  end

end

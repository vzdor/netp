# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


bob = User.find_or_initialize_by(username: 'bob', starting_balance: -100, deduction: 50)
if bob.new_record?
  bob.payments.build(amount: 100, created_at: 1.month.ago)
  bob.payments.build(amount: 50)
  bob.save!
end

User.find_or_create_by(username: 'john', starting_balance: -200, deduction: 50)

User.find_or_create_by(username: 'pepe', starting_balance: -430, deduction: 25, is_disabled: true)

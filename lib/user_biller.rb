class UserBiller
  def initialize(expense)
    @expense = expense
  end

  def bill!
    User.enabled.find_each do |user|
      deduction = @expense.deduction

      if deduction == 0
        deduction = [user.deduction, @expense.amount].min
      end

      if deduction > 0
        user.user_expenses
          .create(amount: deduction, expense: @expense)
      end
    end
  end
end

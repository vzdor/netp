class MonthlyBiller
  def self.bill!(amount = 200)
    expense = Expense.last
    if expense
      do_bill(expense.created_at, amount)
    else
      # todo
    end
  end

  def self.do_bill(billed_at, amount)
    TimeRange.new(billed_at, Time.now)
      .each_month do |t|
      expense = Expense.create(amount: amount, created_at: t)
      UserBiller.new(expense)
        .bill!
    end
  end
end

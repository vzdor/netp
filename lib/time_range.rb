class TimeRange < Range
  def each_month
    i = first
    yield i while (i = i.next_month) < last
  end
end

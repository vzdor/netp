class UserDecorator < Decorator
  delegate :username, :balance, :deduction, to: :model

  def status
    ary = []

    if model.is_disabled?
      ary << "disabled"
    else
      ary << "enabled"
    end

    if model.balance < -(model.deduction * 3)
      ary << "freeloader"
    end

    ary.join(', ')
  end
end

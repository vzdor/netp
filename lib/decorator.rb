class Decorator
  attr_reader :model

  delegate :to_param, :to_model, :persisted?, to: :model

  def initialize(model)
    @model = model
  end

  def self.model_name
    to_s
      .sub(/Decorator$/, '')
      .constantize
      .model_name
  end

  def self.from_ary(models)
    models.map { |model| new(model) }
  end

end
